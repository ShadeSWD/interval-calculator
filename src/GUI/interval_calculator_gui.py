import tkinter as tk
from tkinter import ttk
from src.scripts.interval import Interval
from src.GUI.values_gui import FrmInterval


class MainWindow(tk.Tk):
    """Main window of the program"""

    def __init__(self) -> None:
        """Creates new instance of the program window"""
        super().__init__()
        self.title("Интервальный калькулятор")

        self.frm_btns = tk.Frame(master=self)
        self.btn_calc = BtnEnter(frm=self.frm_btns)
        self.calcs_frame = FrmCalcs(frm=self)
        self.btn_new_calc = BtnNewCalc(frm=self.frm_btns, frm_calcs=self.calcs_frame)

        self.frm_btns.grid(column=0, row=0)
        self.btn_calc.show(col=0, row=0)
        self.btn_new_calc.show(col=1, row=0)


class FrmCalcs:
    """Frame, containing all calculators"""

    def __init__(self, frm) -> None:
        """
        Creates a new instance of frame with calculators
        :param frm: parent frame, instance of tk
        """
        self.parent_frm = frm
        self.frm_calcs = tk.Frame(master=self.parent_frm)

    def create_calc(self) -> None:
        """Creates a new instance of calculator inside this"""
        FrmCalc(frm=self.frm_calcs)

    def show(self, col: int, row: int) -> None:
        """
        Shows this frame inside a parent frame
        :param col: column in a parent frame
        :param row: row in a parent frame
        """
        self.frm_calcs.grid(column=col, row=row)
        for num, calc in enumerate(FrmCalc.all):
            calc.show(col=0, row=num)


class FrmCalc:
    """Frame with an instance of a calculator"""
    all = []

    def __init__(self, frm) -> None:
        """
        Creates a new instance of frame with one calculator
        :param frm: parent frame, instance of tk
        """
        self.all.append(self)

        self.parent_frm = frm
        self.frm_calc = tk.Frame(master=self.parent_frm)

        self.first_interval = FrmInterval(frm=self.frm_calc)
        self.first_interval.show_input(col=0, row=0)

        self.action = FrmAction(frm=self.frm_calc)
        self.action.show(col=1, row=0)

        self.second_interval = FrmInterval(frm=self.frm_calc)
        self.second_interval.show_input(col=2, row=0)

        self.lbl_eq = tk.Label(master=self.frm_calc, text='=')
        self.lbl_eq.grid(column=3, row=0)

        self.output_interval = FrmInterval(frm=self.frm_calc)
        self.output_interval.show_output(col=4, row=0)

    def show(self, col, row) -> None:
        """
        Shows this frame inside a parent frame
        :param col: column in a parent frame
        :param row: row in a parent frame
        """
        self.frm_calc.grid(column=col, row=row)

    def calculate(self):
        """Performs calculations with intervals"""
        interval1 = self.first_interval.get_value()
        interval2 = self.second_interval.get_value()

        action = self.action.tv_action.get()
        if action == '+':
            result = interval1 + interval2
        elif action == '-':
            result = interval1 - interval2
        elif action == '*':
            result = interval1 * interval2
        elif action == '/':
            result = interval1 / interval2
        else:
            result = Interval()

        self.output_interval.lbl_bottom.configure(text=round(result.bottom, 3))
        self.output_interval.lbl_ceiling.configure(text=round(result.ceiling, 3))


class BtnEnter:
    """Button for starting calculations"""

    def __init__(self, frm) -> None:
        """
        Creates a new instance of button that induces calculations
        :param frm: parent frame, instance of tk
        """
        self.btn_calculate = tk.Button(master=frm, text="Рассчитать", command=self.calculate)

    def show(self, col, row) -> None:
        """
        Shows this frame inside a parent frame
        :param col: column in a parent frame
        :param row: row in a parent frame
        """
        self.btn_calculate.grid(column=col, row=row)

    @staticmethod
    def calculate() -> None:
        """Runs calculate function in every calculator"""
        for calculator in FrmCalc.all:
            calculator.calculate()


class BtnNewCalc:
    """Button for starting calculations"""

    def __init__(self, frm, frm_calcs) -> None:
        """
        Creates a new instance of frame with one calculator
        :param frm: parent frame, instance of tk
        :param frm_calcs: frame that contains calculators
        """
        self.parent_frm = frm
        self.frm_calcs = frm_calcs
        self.btn_new_calc = tk.Button(master=self.parent_frm, text="Добавить калькулятор",
                                      command=self.create_calc)

    def show(self, col, row) -> None:
        """
        Shows this frame inside a parent frame
        :param col: column in a parent frame
        :param row: row in a parent frame
        """
        self.btn_new_calc.grid(column=col, row=row)

    def create_calc(self) -> None:
        """function for button to update form and create new calculator"""
        self.frm_calcs.create_calc()
        self.frm_calcs.show(col=0, row=1)


class FrmAction:
    """Button for choosing calculating action"""
    choices = ['+', '-', '*', '/']

    def __init__(self, frm) -> None:
        """
        Creates a new instance of combobox with actions for calculator
        :param frm: parent frame, instance of tk
        """
        self.parent_frm = frm
        self.tv_action = tk.StringVar()
        self.cbbx = ttk.Combobox(master=self.parent_frm, values=self.choices, width=2, state='readonly',
                                 textvariable=self.tv_action)
        self.cbbx.grid(column=0, row=0)

    def show(self, col, row) -> None:
        """
        Shows this frame inside a parent frame
        :param col: column in a parent frame
        :param row: row in a parent frame
        """
        self.cbbx.grid(column=col, row=row)
