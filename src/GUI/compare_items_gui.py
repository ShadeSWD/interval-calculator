import tkinter as tk
from tkinter import ttk, filedialog
from os import path
from src.scripts.item import MasterItem
from src.GUI.values_gui import FrmInterval, FrmFloatValue
from src.scripts.utils import validate_name_length, check_if_float

DATA_DIR = path.join('data')
NAME_WIDTH = 20
SECTION_PAD = 10
BTN_PAD = 5


class MainWindow(tk.Tk):
    """Main window of the program"""
    all = []
    input_table_col = 0
    input_table_row = 1

    output_table_col = 0
    output_table_row = 2

    def __init__(self) -> None:
        """Creates new instance of the program window"""
        super().__init__()
        self.all.append(self)
        self.title("Сравнение объектов")

        self.scroll_x = tk.Scrollbar(self, orient=tk.HORIZONTAL)
        self.scroll_y = tk.Scrollbar(self, orient=tk.VERTICAL)
        self.canvas = tk.Canvas(self, xscrollcommand=self.scroll_x.set, yscrollcommand=self.scroll_y.set)
        self.scroll_x.config(command=self.canvas.xview)
        self.scroll_y.config(command=self.canvas.yview)

        self.main_frm = tk.Frame(master=self.canvas)

        self.canvas.create_window((0, 0), window=self.main_frm,
                                  anchor="nw")

        self.input_table = FrmTableCreation(frm=self.main_frm)
        self.frm_header = tk.Frame(master=self.main_frm, padx=SECTION_PAD, pady=SECTION_PAD)
        self.btn_calculate = BtnCalculate(frm=self.frm_header)
        self.btn_open_from_file = BtnOpenFromFile(frm=self.frm_header)
        self.btn_save_to_file = BtnSaveToFile(frm=self.frm_header)
        self.frm_output = tk.Frame(master=self.main_frm)

        self.scroll_x.pack(fill=tk.X, side=tk.BOTTOM, expand=False)
        self.canvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)
        self.scroll_y.pack(fill=tk.Y, side=tk.RIGHT, expand=False)

        self.frm_header.grid(column=0, row=0, sticky='nw')
        self.btn_calculate.show(col=0, row=0)
        self.btn_open_from_file.show(col=1, row=0)
        self.btn_save_to_file.show(col=2, row=0)
        self.input_table.show(col=self.input_table_col, row=self.input_table_row)

        self.bind("<Configure>", self.resize)
        self.update_idletasks()
        self.minsize(self.winfo_width(), self.winfo_height())

    def resize(self, event):
        region = self.canvas.bbox(tk.ALL)
        self.canvas.configure(scrollregion=region)

    def refresh_input_table(self):
        self.input_table.frm_main.destroy()
        self.input_table = FrmTableCreation(frm=self.main_frm)

    def show_output_table(self):
        self.frm_output.destroy()
        self.frm_output = tk.Frame(master=self.main_frm)
        self.frm_output.grid(column=self.output_table_col, row=self.output_table_row, sticky='nw')
        output_table = FrmTableOutput(frm=self.frm_output)
        output_table.show(col=0, row=0,
                          master_item=FrmTableCreation.all[0].master_item)

        score_table = FrmTableScores(frm=self.frm_output)
        score_table.show(col=0, row=1,
                         master_item=FrmTableCreation.all[0].master_item)


class FrmTableScores:
    """A table for showing scores of all items"""

    def __init__(self, frm):
        self.parent_frm = frm

    def show(self, col: int, row: int, master_item) -> None:
        """
        Shows a frame of master
        :param col: column in parent
        :param row: row in parent
        :param master_item: instance of full item description
        """

        try:
            overall_domination = master_item.get_overall_item_domination()
            sorted_overall_domination = {key: overall_domination[key] for key in
                                         sorted(overall_domination, key=overall_domination.get, reverse=True)}
        except TypeError:
            return

        frm_main = tk.Frame(master=self.parent_frm, padx=SECTION_PAD, pady=SECTION_PAD)
        frm_main.grid(column=col, row=row, sticky='w')

        lbl_header_item_name = tk.Label(master=frm_main, text='Название')
        lbl_header_score = tk.Label(master=frm_main, text='Результат')

        lbl_header_item_name.grid(column=0, row=0)
        lbl_header_score.grid(column=1, row=0, columnspan=2)

        try:
            current_row = 1

            for domination in sorted_overall_domination.keys():
                item = None
                for item_it in master_item.items:
                    if item_it.item_id == domination:
                        item = item_it
                        break
                lbl_item_name = tk.Label(master=frm_main, text=item.name, width=NAME_WIDTH)
                lbl_item_name.grid(column=0, row=current_row)

                pgb_item_score = ttk.Progressbar(orient='horizontal', length=200,
                                                 value=sorted_overall_domination[item.item_id] * 100, master=frm_main)
                pgb_item_score.grid(column=1, row=current_row)

                lbl_item_value = tk.Label(master=frm_main, text=round(sorted_overall_domination[item.item_id], 3))
                lbl_item_value.grid(column=2, row=current_row)

                current_row += 1
        except TypeError:
            frm_main.destroy()
            return


class FrmTableOutput:
    """A table for showing calculated domination matrix"""

    def __init__(self, frm):
        self.parent_frm = frm

    def show(self, col: int, row: int, master_item) -> None:
        """
        Shows a frame of master
        :param col: column in parent
        :param row: row in parent
        :param master_item: instance of full item description
        """

        try:
            domination_matrix = master_item.get_domination_matrix()
        except TypeError:
            return

        frm_main = tk.Frame(master=self.parent_frm, padx=SECTION_PAD, pady=SECTION_PAD)
        lbl_hint = tk.Label(master=frm_main, text='Результат вычислений:')

        frm_main.grid(column=col, row=row, sticky='w')

        try:
            current_row = 0
            lbl_hint.grid(column=0, row=0, columnspan=10, sticky='w')

            current_row += 1

            lbl_item_name = tk.Label(master=frm_main, text=master_item.name, width=NAME_WIDTH)
            lbl_item_name.grid(column=0, row=current_row)

            lbl_weight_coefficient_name = tk.Label(master=frm_main, text='Весовой коэфф.')
            lbl_weight_coefficient_name.grid(column=0, row=current_row + 1)

            header_col = 1
            total_weight = master_item.get_total_weight()
            for master_field in master_item.fields:
                lbl_field_name = tk.Label(master=frm_main, text=master_field.name, width=NAME_WIDTH)
                lbl_field_name.grid(column=header_col, row=current_row)
                lbl_weight_coefficient = tk.Label(master=frm_main,
                                                  text=round(master_field.get_normalised_weight_coefficient(
                                                      total_weight=total_weight), 3), width=NAME_WIDTH)
                lbl_weight_coefficient.grid(column=header_col, row=current_row + 1)
                header_col += 1
            current_row += 2

            for item in master_item.items:
                if not item.is_active:
                    continue
                line_col = 0
                lbl_item_name = tk.Label(master=frm_main, text=item.name, width=NAME_WIDTH)
                lbl_item_name.grid(column=line_col, row=current_row)
                line_col += 1

                for master_field in master_item.fields:
                    for line in domination_matrix:
                        frm_line_val = tk.Frame(master=frm_main)
                        frm_line_val.grid(column=line_col, row=current_row)
                        if line['item_id'] == item.item_id and line['parameter_id'] == master_field.parameter_id:
                            pgb_item_score = ttk.Progressbar(orient='horizontal', length=100,
                                                             value=line['value'] * 100, master=frm_line_val)
                            pgb_item_score.grid(column=0, row=0)
                            lbl_item_domination = tk.Label(master=frm_line_val, text=round(line['value'], 3), width=4)
                            lbl_item_domination.grid(column=1, row=0)
                    line_col += 1
                current_row += 1
        except TypeError:
            frm_main.destroy()
            return


class FrmTableCreation:
    """A table for master item and its variants"""
    all = []

    def __init__(self, frm) -> None:
        """
        Creates an instance of input table
        :param frm: parent frame
        """
        try:
            self.all[0] = self
        except IndexError:
            self.all.append(self)
        self.master_fields = {}
        self.items = {}

        self.frm_main = tk.Frame(master=frm, padx=SECTION_PAD, pady=SECTION_PAD)
        self.vcmd_tl = (frm.register(validate_name_length), '%P')
        self.master_item = MasterItem()

        self.lbl_hint = tk.Label(master=self.frm_main, text='Введите исходные данные:')

        self.tv_master_name = tk.StringVar()
        self.entry_master_name = tk.Entry(master=self.frm_main, width=NAME_WIDTH, textvariable=self.tv_master_name,
                                          validate='key', validatecommand=self.vcmd_tl)

        self.lbl_field_type = tk.Label(master=self.frm_main, text='Тип поля')
        self.lbl_goal_value = tk.Label(master=self.frm_main, text='Целевое значение')
        self.lbl_weight_coefficient = tk.Label(master=self.frm_main, text='Весовой коэффициент')
        self.lbl_max_value = tk.Label(master=self.frm_main, text='Макс. значение')

        self.btn_add_row = BtnAddRow(frm=self.frm_main)
        self.btn_add_col = BtnAddCol(frm=self.frm_main)

    def update(self, col: int, row: int) -> None:
        self.configure_master_item()
        self.show(col=col, row=row)

    def configure_master_item(self):
        self.master_item.name = self.tv_master_name.get()
        for master_field_id, master_field in self.master_fields.items():
            for field in self.master_item.fields:
                if field.parameter_id == master_field_id:
                    field.name = master_field.tv_field_name.get()
                    field.value_type = master_field.get_value_type()
                    field.goal_value_type = master_field.get_goal_value_type()
                    field.weight_coefficient = master_field.tv_weight_coefficient.get()
                    field.max_value = master_field.tv_max_value.get()
                    break

        for item_field_id, line_item in self.items.items():
            for item in self.master_item.items:
                if item.item_id == item_field_id:
                    item.name = line_item.tv_item_name.get()
                    item.is_active = line_item.bv_item_is_active.get()

                    for param_id, param in line_item.item_fields.items():
                        for item_field in item.fields:
                            if item_field.parameter_id == param_id:
                                try:
                                    item_field.value = param.entry_item_value.get_value()
                                except AttributeError:
                                    pass
                                break
                    break

    def show(self, col: int, row: int) -> None:
        """
        Shows a frame of master
        :param col: column in parent
        :param row: row in parent
        """

        self.frm_main.grid(column=col, row=row, sticky='nw')
        self.lbl_hint.grid(column=0, row=0, columnspan=10, sticky='w')

        self.tv_master_name.set(self.master_item.name)
        self.entry_master_name.grid(column=0, row=1)
        self.lbl_field_type.grid(column=0, row=2)
        self.lbl_max_value.grid(column=0, row=3)
        self.lbl_goal_value.grid(column=0, row=4, rowspan=2)
        self.lbl_weight_coefficient.grid(column=0, row=6)

        last_row = 7
        last_col = 1
        col = last_col

        for master_item_field in self.master_item.fields:
            try:
                master_field = self.master_fields[master_item_field.parameter_id]
            except KeyError:
                master_field = MasterField(frm=self.frm_main)
                self.master_fields[master_item_field.parameter_id] = master_field

            master_field.tv_field_name.set(master_item_field.name)
            master_field.entry_field_name.grid(column=col, row=1)

            try:
                master_field.tv_field_type.set(MasterField.value_types[master_item_field.value_type])
            except KeyError:
                pass
            master_field.cbbx_field_type.grid(column=col, row=2)

            if master_item_field.max_value:
                master_field.tv_max_value.set(master_item_field.max_value)
            master_field.entry_max_value.grid(column=col, row=3)

            try:
                master_field.tv_goal_value_type.set(MasterField.goal_value_types[master_item_field.goal_value_type])
            except KeyError:
                pass
            master_field.cbbx_goal_value.grid(column=col, row=4)

            master_field.lbl_goal_value.configure(text=master_item_field.get_goal_value(items=self.master_item.items))
            master_field.lbl_goal_value.grid(column=col, row=5)

            if master_item_field.weight_coefficient:
                master_field.tv_weight_coefficient.set(master_item_field.weight_coefficient)
            master_field.entry_weight_coefficient.grid(column=col, row=6)
            col += 1

        row = last_row

        for sub_item in self.master_item.items:
            try:
                item = self.items[sub_item.item_id]
            except KeyError:
                item = ItemLine(frm=self.frm_main)
                self.items[sub_item.item_id] = item

            item.tv_item_name.set(sub_item.name)
            item.bv_item_is_active.set(sub_item.is_active)
            item.frm_item_entry.grid(column=0, row=row)

            col = last_col
            for item_param in sub_item.fields:
                value_type = None
                for fld in self.master_item.fields:
                    if fld.parameter_id == item_param.parameter_id:
                        value_type = fld.value_type
                        break

                try:
                    param = item.item_fields[item_param.parameter_id]
                    if param.value_type != value_type:
                        try:
                            param.entry_item_value.destroy()
                        except AttributeError:
                            pass
                        raise KeyError
                except KeyError:
                    param = ItemValue(frm=self.frm_main, value_type=value_type)
                    item.item_fields[item_param.parameter_id] = param

                if param.entry_item_value:
                    param.entry_item_value.set_value(item_param.value)
                    param.entry_item_value.show_input(col=col, row=row)
                col += 1
            row += 1

        last_col = col
        last_row = row

        self.btn_add_row.show(col=0, row=last_row)
        self.btn_add_col.show(col=last_col, row=1)


class MasterField:
    """Fields for master parameter"""
    value_types = {1: 'Число', 2: 'Интервал'}
    goal_value_types = {1: 'Минимум', 2: 'Максимум'}

    def __init__(self, frm) -> None:
        """
        Creates an instance with master parameter fields
        :param frm: parent frame
        """
        self.vcmd_tl = (frm.register(validate_name_length), '%P')
        self.vcmd_float = (frm.register(check_if_float), '%P')

        self.tv_field_name = tk.StringVar()
        self.entry_field_name = tk.Entry(master=frm, width=NAME_WIDTH, textvariable=self.tv_field_name, validate='key',
                                         validatecommand=self.vcmd_tl)

        self.tv_field_type = tk.StringVar()
        self.cbbx_field_type = ttk.Combobox(master=frm, values=[val for val in self.value_types.values()],
                                            textvariable=self.tv_field_type, state='readonly', width=NAME_WIDTH - 5)

        self.tv_max_value = tk.StringVar()
        self.entry_max_value = tk.Entry(master=frm, width=NAME_WIDTH - 5,
                                        textvariable=self.tv_max_value,
                                        validate='key', validatecommand=self.vcmd_float)

        self.tv_goal_value_type = tk.StringVar()
        self.cbbx_goal_value = ttk.Combobox(master=frm, values=[val for val in self.goal_value_types.values()],
                                            textvariable=self.tv_goal_value_type, state='readonly',
                                            width=NAME_WIDTH - 5)

        self.lbl_goal_value = tk.Label(master=frm, text='')

        self.tv_weight_coefficient = tk.StringVar()
        self.entry_weight_coefficient = tk.Entry(master=frm, width=NAME_WIDTH - 5,
                                                 textvariable=self.tv_weight_coefficient,
                                                 validate='key', validatecommand=self.vcmd_float)

    def get_value_type(self) -> int:
        try:
            return list(self.value_types.keys())[list(self.value_types.values()).index(self.tv_field_type.get())]
        except ValueError:
            return 0

    def get_goal_value_type(self) -> int:
        try:
            return list(self.goal_value_types.keys())[list(
                self.goal_value_types.values()).index(self.tv_goal_value_type.get())]
        except ValueError:
            return 0


class ItemLine:
    """Fields for item parameters"""

    def __init__(self, frm) -> None:
        """
        Creates an instance of item line
        :param frm: parent frame
        """
        self.frm_item_entry = tk.Frame(master=frm)

        self.item_fields = {}
        self.tv_item_name = tk.StringVar()
        self.entry_item_name = tk.Entry(master=self.frm_item_entry, width=NAME_WIDTH, textvariable=self.tv_item_name)
        self.entry_item_name.grid(column=0, row=0)

        self.bv_item_is_active = tk.BooleanVar()
        self.bv_item_is_active.set(True)
        self.chckbtn_item_is_active = ttk.Checkbutton(master=self.frm_item_entry, variable=self.bv_item_is_active)
        self.chckbtn_item_is_active.grid(column=1, row=0)


class ItemValue:
    """Field for value of an item parameter"""

    def __init__(self, frm, value_type: int) -> None:
        """
        Creates an instance of item value
        :param frm: parent frame
        :param value_type: value_type: {0: 'none', 1: 'float', 2: 'interval
        """

        self.value_type = value_type
        if self.value_type == 0:
            self.entry_item_value = None
        elif self.value_type == 1:
            self.entry_item_value = FrmFloatValue(frm=frm)
        elif self.value_type == 2:
            self.entry_item_value = FrmInterval(frm=frm)


class BtnAddRow:
    """Button for adding a new variant"""

    def __init__(self, frm) -> None:
        """
        Creates an instance of a button
        :param frm: parent frame
        """

        self.frm_main = tk.Frame(master=frm)
        self.btn_add_row = tk.Button(master=self.frm_main, text='+', command=self.command)
        self.btn_add_row.grid(column=0, row=0)

    def show(self, col: int, row: int) -> None:
        """
        Shows a button in parent
        :param col: column in parent
        :param row: row in parent
        """

        self.frm_main.grid(column=col, row=row)

    @staticmethod
    def command():
        FrmTableCreation.all[0].master_item.add_item()
        FrmTableCreation.all[0].update(col=MainWindow.input_table_col, row=MainWindow.input_table_row)


class BtnAddCol:
    """Button for adding a new property"""

    def __init__(self, frm) -> None:
        """
        Creates an instance of a button
        :param frm: parent frame
        """

        self.frm_main = tk.Frame(master=frm)
        self.btn_add_col = tk.Button(master=self.frm_main, text='+', command=self.command)
        self.btn_add_col.grid(column=0, row=0)

    def show(self, col: int, row: int) -> None:
        """
        Shows a button in parent
        :param col: column in parent
        :param row: row in parent
        """

        self.frm_main.grid(column=col, row=row)

    @staticmethod
    def command():
        FrmTableCreation.all[0].master_item.add_field()
        FrmTableCreation.all[0].update(col=MainWindow.input_table_col, row=MainWindow.input_table_row)


class BtnCalculate:
    """Button to perform calculations"""

    def __init__(self, frm) -> None:
        """
        Creates an instance of a button
        :param frm: parent frame
        """

        self.frm_main = tk.Frame(master=frm, padx=BTN_PAD, pady=BTN_PAD)
        self.btn_calculate = tk.Button(master=self.frm_main, text='Рассчитать', command=self.command)
        self.btn_calculate.grid(column=0, row=0)

    def show(self, col: int, row: int) -> None:
        """
        Shows a button in parent
        :param col: column in parent
        :param row: row in parent
        """

        self.frm_main.grid(column=col, row=row, sticky='w')

    @staticmethod
    def command():
        FrmTableCreation.all[0].update(col=MainWindow.input_table_col, row=MainWindow.input_table_row)
        MainWindow.all[0].show_output_table()


class BtnSaveToFile:
    """Button to save configuration"""

    def __init__(self, frm) -> None:
        """
        Creates an instance of a button
        :param frm: parent frame
        """

        self.frm_main = tk.Frame(master=frm, padx=BTN_PAD, pady=BTN_PAD)
        self.btn_save = tk.Button(master=self.frm_main, text='Сохранить конфигурацию', command=self.command)
        self.btn_save.grid(column=0, row=0)

    def show(self, col: int, row: int) -> None:
        """
        Shows a button in parent
        :param col: column in parent
        :param row: row in parent
        """

        self.frm_main.grid(column=col, row=row, sticky='w')

    @staticmethod
    def choose_file():
        filetypes = (("JSON", "*.json"), ("Все файлы", "*"),)
        filename = filedialog.askopenfilename(title="Выбрать файл", initialdir=DATA_DIR, filetypes=filetypes)
        return filename

    def command(self):
        filename = self.choose_file()
        FrmTableCreation.all[0].update(col=MainWindow.input_table_col, row=MainWindow.input_table_row)
        MainWindow.all[0].show_output_table()
        FrmTableCreation.all[0].master_item.save_to_file(file=filename)


class BtnOpenFromFile:
    """Button to open configuration"""

    def __init__(self, frm) -> None:
        """
        Creates an instance of a button
        :param frm: parent frame
        """

        self.frm_main = tk.Frame(master=frm, padx=BTN_PAD, pady=BTN_PAD)
        self.btn_open = tk.Button(master=self.frm_main, text='Открыть конфигурацию', command=self.command)
        self.btn_open.grid(column=0, row=0)

    def show(self, col: int, row: int) -> None:
        """
        Shows a button in parent
        :param col: column in parent
        :param row: row in parent
        """

        self.frm_main.grid(column=col, row=row, sticky='w')

    @staticmethod
    def choose_file():
        filetypes = (("JSON", "*.json"), ("Все файлы", "*"),)
        filename = filedialog.askopenfilename(title="Выбрать файл", initialdir=DATA_DIR, filetypes=filetypes)
        return filename

    def command(self):
        filename = self.choose_file()
        MainWindow.all[0].refresh_input_table()
        FrmTableCreation.all[0].master_item = MasterItem().instantiate_from_json(file=filename)
        FrmTableCreation.all[0].show(col=MainWindow.input_table_col, row=MainWindow.input_table_row)
        FrmTableCreation.all[0].update(col=MainWindow.input_table_col, row=MainWindow.input_table_row)
        MainWindow.all[0].show_output_table()
