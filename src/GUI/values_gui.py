import tkinter as tk
from src.scripts.utils import check_if_float
from src.scripts.interval import Interval


class FrmFloatValue:
    """One input field for a float value"""
    entry_width = 10

    def __init__(self, frm) -> None:
        """
        Creates a new instance of interval
        :param frm: parent frame, instance of tk
        """

        self.parent_frm = frm
        self.frm_number = tk.Frame(master=self.parent_frm)

        self.vcmd = (frm.register(check_if_float), '%P')

        self.tv_entry_number = tk.StringVar()
        self.entry_number = tk.Entry(master=self.frm_number, width=self.entry_width,
                                     textvariable=self.tv_entry_number, validate='key', validatecommand=self.vcmd)

        self.lbl_number = tk.Label(master=self.frm_number, width=self.entry_width)

    def get_value(self):
        """Creates instance from gui data"""
        try:
            return float(self.tv_entry_number.get())
        except ValueError:
            return

    def show_input(self, col, row) -> None:
        """
        Shows this frame inside a parent frame
        :param col: column in a parent frame
        :param row: row in a parent frame
        """
        self.frm_number.grid(column=col, row=row)
        self.entry_number.grid(column=0, row=0)

    def show_output(self, col, row) -> None:
        """
        Shows this frame inside a parent frame
        :param col: column in a parent frame
        :param row: row in a parent frame
        """

        self.frm_number.grid(column=col, row=row)
        self.lbl_number.grid(column=0, row=0)

    def destroy(self):
        self.frm_number.destroy()

    def set_value(self, value):
        if value:
            self.tv_entry_number.set(value)


class FrmInterval:
    """Field with 2 inputs or labels for bottom and ceiling of an interval"""
    entry_width = 10

    def __init__(self, frm) -> None:
        """
        Creates a new instance of interval
        :param frm: parent frame, instance of tk
        """

        self.parent_frm = frm

        self.frm_interval = tk.Frame(master=self.parent_frm)
        self.vcmd = (self.frm_interval.register(check_if_float), '%P')

        self.lbl_open_bracket = tk.Label(master=self.frm_interval, text='[')
        self.lbl_close_bracket = tk.Label(master=self.frm_interval, text=']')
        self.lbl_sep_and_tab = tk.Label(master=self.frm_interval, text=', ')

        self.tv_bottom = tk.StringVar()
        self.tv_ceiling = tk.StringVar()
        self.entry_bottom = tk.Entry(master=self.frm_interval, width=self.entry_width, textvariable=self.tv_bottom,
                                     validate='key', validatecommand=self.vcmd)
        self.entry_ceiling = tk.Entry(master=self.frm_interval, width=self.entry_width, textvariable=self.tv_ceiling,
                                      validate='key', validatecommand=self.vcmd)

        self.lbl_bottom = tk.Label(master=self.frm_interval, width=self.entry_width)
        self.lbl_ceiling = tk.Label(master=self.frm_interval, width=self.entry_width)

    def show_input(self, col, row) -> None:
        """
        Shows this frame inside a parent frame
        :param col: column in a parent frame
        :param row: row in a parent frame
        """

        self.frm_interval.grid(column=col, row=row)
        self.lbl_open_bracket.grid(column=0, row=0)
        self.entry_bottom.grid(column=1, row=0)
        self.lbl_sep_and_tab.grid(column=2, row=0)
        self.entry_ceiling.grid(column=3, row=0)
        self.lbl_close_bracket.grid(column=4, row=0)

    def show_output(self, col, row) -> None:
        """
        Shows this frame inside a parent frame
        :param col: column in a parent frame
        :param row: row in a parent frame
        """

        self.frm_interval.grid(column=col, row=row)
        self.lbl_open_bracket.grid(column=0, row=0)
        self.lbl_bottom.grid(column=1, row=0)
        self.lbl_sep_and_tab.grid(column=2, row=0)
        self.lbl_ceiling.grid(column=3, row=0)
        self.lbl_close_bracket.grid(column=4, row=0)

    def get_value(self):
        """Creates instance from gui data"""
        try:
            return Interval(bottom=float(self.tv_bottom.get()),
                            ceiling=float(self.tv_ceiling.get()))
        except ValueError:
            return

    def destroy(self):
        self.frm_interval.destroy()

    def set_value(self, value):
        if value:
            self.tv_bottom.set(value.bottom)
            self.tv_ceiling.set(value.ceiling)
