from src.scripts.conversions import string_to_float
import json

from src.scripts.interval import Interval


class MasterItem:
    """Master item, that contains a description of the object in general"""

    def __init__(self, name: str = None, fields: list = []) -> None:
        """
        Creates an instance of master item
        :param name: name of an item in general
        :param fields: fields that can be in the item
        """

        self.field_ids = 0
        self.item_ids = 0
        self.name = name
        self.fields = fields
        self.items = []

    def __str__(self):
        """Representation in a string"""
        return f'{self.name}'

    def __repr__(self):
        """Full representation"""
        return f'MasterItem(name="{self.name}", fields={", ".join([repr(field) for field in self.fields])}, ' \
               f'items={", ".join([repr(item) for item in self.items])})'

    def serialize(self) -> dict:
        """Returns attributes"""
        item = {
            'name': self.name,
            'fields': [field.serialize() for field in self.fields],
            'items': [item.serialize() for item in self.items]
        }
        return item

    @classmethod
    def deserialize(cls, data: dict):
        """Creates an instance of Master Item"""
        items = []
        for item in data['items']:
            items.append(Item.deserialize(data=item))

        fields = []
        for field in data['fields']:
            fields.append(MasterParameter.deserialize(data=field))

        master_item = cls(
            name=data['name'],
            fields=fields
        )

        master_item.items = items
        master_item.field_ids = len(fields)
        master_item.item_ids = len(items)

        return master_item

    def save_to_file(self, file):
        if file.endswith('.json'):
            self.save_to_json(file)
        else:
            pass

    def save_to_json(self, file):
        data = self.serialize()
        with open(file, "w+") as json_file:
            json.dump(obj=data, indent=2, fp=json_file)

    def instantiate_from_json(self, file):
        with open(file, "r") as json_file:
            data = json.load(fp=json_file)
            return self.deserialize(data=data)

    def add_field(self, name: str = None, value_type: int = 0, goal_value: int | float = None,
                  weight_coefficient: int | float = None) -> None:
        """
        Creates a new field and adds it to the sub items
        :param name: name of the parameter
        :param value_type: type of the parameter {0: 'float', 1: 'interval'}
        :param goal_value: goal value of the parameter
        :param weight_coefficient: weight of the parameter
        """

        new_id = self.field_ids
        self.field_ids += 1
        new_field = MasterParameter(parameter_id=new_id, name=name, value_type=value_type, goal_value_type=goal_value,
                                    weight_coefficient=weight_coefficient)
        self.fields.append(new_field)

        for item in self.items:
            item.add_parameter(parameter_id=new_id)

    def add_item(self):
        """Adds new empty item to the main item"""
        new_item = Item(fields=self.fields, item_id=self.item_ids)
        self.item_ids += 1
        self.items.append(new_item)

    def get_total_weight(self):
        """Calculates total weight of all parameters"""
        total_weight = 0
        for param in self.fields:
            total_weight += param.weight_coefficient
        return total_weight

    def get_max_weight(self):
        """Calculates maximum weight of all parameters"""
        weights = []
        for param in self.fields:
            weights.append(param.weight_coefficient)
        return max(weights)

    def get_scoring_matrices(self) -> dict:
        """Gets scoring matrices for each parameter"""
        matrices = {}
        for field in self.fields:
            matrix = field.get_scoring_matrix(items=self.items)
            matrices[field.parameter_id] = matrix
        return matrices

    def get_domination_matrix(self) -> list:
        """Gets domination matrix for all items"""
        scoring_matrices = self.get_scoring_matrices()
        domination_matrix = []

        for field in self.fields:
            field_matrix = scoring_matrices[field.parameter_id]

            # Search for minimum value
            if field.goal_value_type == 1:
                for item in self.items:
                    if item.is_active:
                        scores = []
                        for score in field_matrix:
                            if score['item_line'] == item.item_id:
                                scores.append(score['value'])
                        value = min(scores)
                        domination_matrix.append({'item_id': item.item_id, 'parameter_id': field.parameter_id,
                                                  'value': value})

            # Search for maximum value
            elif field.goal_value_type == 2:
                for item in self.items:
                    if item.is_active:
                        scores = []
                        for score in field_matrix:
                            if score['item_column'] == item.item_id:
                                scores.append(score['value'])
                        value = min(scores)
                        domination_matrix.append({'item_id': item.item_id, 'parameter_id': field.parameter_id,
                                                  'value': value})
        return domination_matrix

    def get_overall_item_domination(self) -> dict:
        domination_matrix = self.get_domination_matrix()
        total_weight = self.get_total_weight()
        overall_domination = {}
        for item in self.items:
            if item.is_active:
                item_score = 0
                for line in domination_matrix:
                    if line['item_id'] == item.item_id:
                        for param in self.fields:
                            if param.parameter_id == line['parameter_id']:
                                item_score += line['value'] * param.get_normalised_weight_coefficient(
                                    total_weight=total_weight)
                domination = item_score
                overall_domination[item.item_id] = domination
        return overall_domination


class Item:
    """A particular item with attributes of master"""

    def __init__(self, item_id: int, fields: list, name: str = None, is_active: bool = True) -> None:
        """
        Create an instance of the item
        :param name: name of the item
        :param is_active: include in calculations or not
        :param fields: attributes as in master
        """

        self.item_id = item_id
        self.is_active = is_active
        self.name = name
        self.fields = []
        for field in fields:
            self.add_parameter(parameter_id=field.parameter_id)

    def __str__(self):
        """Representation in a string"""
        return f'{self.name}'

    def __repr__(self):
        """Full representation"""
        return f'Item(item_id={self.item_id}, is_active={self.is_active}, name="{self.name}", ' \
               f'fields={", ".join([repr(field) for field in self.fields])})'

    @classmethod
    def deserialize(cls, data: dict):
        """Creates an instance of Item"""
        params = []
        for field in data['fields']:
            params.append(Parameter.deserialize(data=field))
        item = cls(
            item_id=data['id'],
            name=data['name'],
            is_active=data['is_active'],
            fields=[]
            )
        item.fields = params
        return item

    def serialize(self) -> dict:
        """Returns attributes"""
        item = {
            'id': self.item_id,
            'name': self.name,
            'is_active': self.is_active,
            'fields': [field.serialize() for field in self.fields]
        }
        return item

    def add_parameter(self, parameter_id: int, value=None) -> None:
        """
        Adds a new parameter after it is added to master
        :param parameter_id: id of the master parameter
        :param value: value of the parameter
        """

        new_parameter = Parameter(parameter_id=parameter_id, value=value)
        self.fields.append(new_parameter)


class MasterParameter:
    """A description of the parameter"""
    goal_value_types = {0: 'none', 1: 'min', 2: 'max'}
    value_types = {0: 'none', 1: 'float', 2: 'interval'}

    def __init__(self, parameter_id: int, name: str, value_type: int, goal_value_type: int,
                 weight_coefficient: int | float, max_value: int | float = None) -> None:
        """
        Creates an instance of master parameter
        :param parameter_id: id of the parameter
        :param name: name of the parameter
        :param value_type: {0: 'none', 1: 'float', 2: 'interval'}
        :param goal_value_type: {0: 'none', 1: 'min', 2: 'max'}
        :param weight_coefficient: weight of the parameter
        :param max_value: maximum possible value
        """

        self.parameter_id = parameter_id
        self.name = name
        self.value_type = value_type
        self.goal_value_type = goal_value_type
        self.weight_coefficient = weight_coefficient
        self.max_value = max_value

    def __str__(self):
        """Representation in a string"""
        return f'{self.parameter_id} - {self.name}'

    def __repr__(self):
        """Full representation"""
        return f'MasterParameter(parameter_id={self.parameter_id}, name="{self.name}", ' \
               f'value_type="{self.value_types[self.value_type]}", ' \
               f'goal_value_type={self.goal_value_types[self.goal_value_type]}, ' \
               f'weight_coefficient={self.weight_coefficient})'

    @classmethod
    def deserialize(cls, data: dict):
        """Creates an instance of MasterParameter"""
        field = cls(
            parameter_id=data['id'],
            name=data['name'],
            value_type=data['value_type'],
            goal_value_type=data['goal_value_type'],
            weight_coefficient=data['weight_coefficient'],
            max_value=data['max_value']
        )
        return field

    def serialize(self) -> dict:
        """Returns attributes"""
        parameter = {
            "id": self.parameter_id,
            'name': self.name,
            'value_type': self.value_type,
            'goal_value_type': self.goal_value_type,
            'weight_coefficient': self.weight_coefficient,
            'max_value': self.max_value
        }
        return parameter

    @property
    def value_type(self) -> int:
        """Type of value in the cell {0: 'none', 1: 'float', 2: 'interval'}"""
        return self.__value_type

    @value_type.setter
    def value_type(self, value) -> None:
        if value:
            try:
                value = int(value)
            except ValueError:
                raise Exception(f'Value type must be integer')
        else:
            value = 0
        if value in self.value_types.keys():
            self.__value_type = value
        else:
            raise Exception(f'Unsupported value key {value}')

    @property
    def goal_value_type(self) -> int:
        """Type of value in the cell {0: 'none', 1: 'min', 2: 'max'}"""
        return self.__goal_value

    @goal_value_type.setter
    def goal_value_type(self, value: int | float | str) -> None:
        if value:
            try:
                value = int(value)
            except ValueError:
                raise Exception(f'Goal value type must be integer')
        else:
            value = 0
        if value in self.goal_value_types.keys():
            self.__goal_value = value
        else:
            raise Exception(f'Unsupported value key {value}')

    @property
    def weight_coefficient(self) -> float:
        """Weight of the parameter"""
        return self.__weight_coefficient

    @weight_coefficient.setter
    def weight_coefficient(self, value) -> None:
        self.__weight_coefficient = string_to_float(value)

    @property
    def max_value(self) -> float:
        """Maximum possible value of the parameter"""
        return self.__max_value

    @max_value.setter
    def max_value(self, value):
        self.__max_value = string_to_float(value)

    def get_normalised_weight_coefficient(self, total_weight: int | float) -> float:
        """
        Calculates normalised weight coefficient
        :param total_weight: total weight
        :return: normalised weight
        """

        try:
            return self.weight_coefficient / total_weight
        except ZeroDivisionError:
            return 0

    def get_goal_value(self, items: list) -> float | None:
        """
        Finds a minimum or a maximum value in an item
        :param items: list of instances of Item
        :return: minimum or maximum value
        """

        if self.goal_value_type == 1:
            return self.get_min_value(items=items)
        if self.goal_value_type == 2:
            return self.get_max_value(items=items)
        else:
            return

    def get_min_value(self, items: list) -> float | None:
        """
        Finds a minimum value in an item
        :param items: list of instances of Item
        :return: minimum value
        """

        values = []
        for item in items:
            if item.is_active:
                for field in item.fields:
                    if field.parameter_id == self.parameter_id:
                        if field.value:
                            values.append(field.value)
                        break

        if not values:
            return

        try:
            if self.value_type == 1:
                return min(values)
            if self.value_type == 2:
                return min(interval.bottom for interval in values)
        except AttributeError:
            pass

        else:
            return

    def get_max_value(self, items: list) -> float | None:
        """
        Finds a maximum value in an item
        :param items: list of instances of Item
        :return: maximum value
        """

        if self.max_value:
            return self.max_value

        values = []
        for item in items:
            if item.is_active:
                for field in item.fields:
                    if field.parameter_id == self.parameter_id:
                        if field.value:
                            values.append(field.value)
                        break

        if not values:
            return

        try:
            if self.value_type == 1:
                return max(values)
            if self.value_type == 2:
                return max(interval.ceiling for interval in values)
        except AttributeError:
            pass

        else:
            return

    def get_scoring_matrix(self, items: list) -> list:
        """
        Finds a minimum or a maximum value in an item
        :param items: list of instances of Item
        :return: [{'item_line': item_id, 'item_column': item_id, 'value'}, ...]
        """

        matrix = []
        max_value = self.get_max_value(items=items)

        for item_line in items:
            if item_line.is_active:
                item_line_param_value = None
                for field in item_line.fields:
                    if field.parameter_id == self.parameter_id:
                        item_line_param_value = field.value
                        break

                for item_column in items:
                    if item_column.is_active:
                        item_column_param_value = None
                        for field in item_column.fields:
                            if field.parameter_id == self.parameter_id:
                                item_column_param_value = field.value
                                break
                        value = self.calculate_value_scoring_matrix(item_line_param_value=item_line_param_value,
                                                                    item_column_param_value=item_column_param_value,
                                                                    goal_value=max_value)
                        matrix.append({'item_line': item_line.item_id, 'item_column': item_column.item_id,
                                       'value': value})
        return matrix

    @staticmethod
    def calculate_value_scoring_matrix(item_line_param_value: float, item_column_param_value: float,
                                       goal_value: float) -> float | None:
        """
        Calculates value for scoring matrix
        :param item_line_param_value: value of the parameter in line
        :param item_column_param_value: value of the parameter in column
        :param goal_value: minimum or maximum value
        :return: result
        """

        try:
            value = (item_line_param_value - item_column_param_value) / goal_value
        except TypeError:
            return None

        if isinstance(value, Interval):
            value = value.abs_range_width()

        if value > 0:
            return value * (-1) + 1
        else:
            return value * 0 + 1


class Parameter:
    """Parameter of a specific item"""

    def __init__(self, parameter_id: int, value) -> None:
        self.parameter_id = parameter_id
        self.value = value

    def __str__(self):
        """Representation in a string"""
        return f'{self.parameter_id} - {self.value}'

    def __repr__(self):
        """Full representation"""
        return f'Parameter(parameter_id={self.parameter_id}, value={str(self.value)})'

    def serialize(self) -> dict:
        """Returns attributes"""
        if isinstance(self.value, Interval):
            value = self.value.serialize()
        else:
            value = self.value
        parameter = {
            'id': self.parameter_id,
            'value': value
        }
        return parameter

    @classmethod
    def deserialize(cls, data: dict):
        """Creates an instance of Parameter"""
        value = data['value']
        if isinstance(data['value'], dict):
            keys = [key for key in data['value'].keys()]
            if len(keys) == 2 and 'bottom' in keys and 'ceiling' in keys:
                value = Interval.deserialize(data=data['value'])
        field = cls(
            parameter_id=data['id'],
            value=value
        )
        return field
