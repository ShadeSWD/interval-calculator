class Interval:
    """
    Description of an interval of float numbers
    """
    def __init__(self, bottom: float = None, ceiling: float = None) -> None:
        """
        Initialise new interval

        :param bottom: minimum value of an interval
        :param ceiling: maximum value of an interval
        """
        if bottom and ceiling:
            if bottom > ceiling:
                ceiling_save = ceiling
                ceiling = bottom
                bottom = ceiling_save

        self.bottom = bottom
        if ceiling:
            self.ceiling = ceiling
        else:
            self.ceiling = self.bottom

    def __str__(self) -> str:
        return f'[{self.bottom}, {self.ceiling}]'

    def __repr__(self) -> str:
        return f'Interval(bottom={self.bottom}, ceiling={self.ceiling})'

    def serialize(self) -> dict:
        """Returns attributes"""
        interval = {
            'bottom': self.bottom,
            'ceiling': self.ceiling
        }
        return interval

    @classmethod
    def deserialize(cls, data: dict):
        """Creates an instance of Master Item"""
        interval = cls(
            bottom=data['bottom'],
            ceiling=data['ceiling']
        )
        return interval

    @property
    def range_width(self) -> float:
        """
        Difference between minimum and maximum values

        :return: float number
        """

        range_width = self.ceiling - self.bottom
        return range_width

    def abs_range_width(self) -> float:
        """
        Difference between minimum and maximum values and zero

        :return: float number
        """

        result = Interval(self.bottom, self.ceiling) - Interval(-self.ceiling, -self.bottom)
        range_width = result.bottom
        return range_width

    @property
    def median(self) -> float:
        """
        Medium value of an interval

        :return: float number
        """
        median = (self.ceiling + self.bottom) / 2
        return median

    @classmethod
    def create_interval(cls, bottom: float, ceiling: float):
        """
        Creates a new instance of Interval

        :param bottom: minimum value of an interval
        :param ceiling: maximum value of an interval
        :return: instance of Interval
        """
        return cls(bottom=bottom, ceiling=ceiling)

    def __add__(self, other):
        if not isinstance(other, Interval):
            other = Interval(other)
        bottom = self.bottom + other.bottom
        ceiling = self.ceiling + other.ceiling
        return self.create_interval(bottom=bottom, ceiling=ceiling)

    def __sub__(self, other):
        if not isinstance(other, Interval):
            other = Interval(other)
        differences = [self.bottom - other.bottom, self.ceiling - other.ceiling]
        bottom = min(differences)
        ceiling = max(differences)
        return self.create_interval(bottom=bottom, ceiling=ceiling)

    def __mul__(self, other):
        if not isinstance(other, Interval):
            other = Interval(other)
        multiplications = []
        for number1 in [self.bottom, self.ceiling]:
            for number2 in [other.bottom, other.ceiling]:
                multiplications.append(number1 * number2)
        bottom = min(multiplications)
        ceiling = max(multiplications)
        return self.create_interval(bottom=bottom, ceiling=ceiling)

    def __truediv__(self, other):
        if not isinstance(other, Interval):
            other = Interval(other)
        # Unfinished!
        try:
            if self >= 0 and other > 0:
                divisions = [self.bottom / other.bottom, self.ceiling / other.ceiling]
                bottom = min(divisions)
                ceiling = max(divisions)
                return self.create_interval(bottom=bottom, ceiling=ceiling)
        except Exception:
            pass

        try:
            if self <= 0 and other < 0:
                divisions = [self.bottom / other.ceiling, self.ceiling / other.bottom]
                bottom = min(divisions)
                ceiling = max(divisions)
                return self.create_interval(bottom=bottom, ceiling=ceiling)
        except Exception:
            pass

        try:
            if not self.number_is_in_interval(number=0) and not other.number_is_in_interval(number=0):
                divisions = [self.bottom / other.ceiling, self.ceiling / other.bottom, self.bottom / other.bottom,
                             self.ceiling / other.ceiling]
                bottom = min(divisions)
                ceiling = max(divisions)
                return self.create_interval(bottom=bottom, ceiling=ceiling)
        except Exception:
            pass

        try:
            if self.number_is_in_interval(number=0) and other > 0:
                bottom = self.bottom / other.bottom
                ceiling = self.ceiling / other.bottom
                return self.create_interval(bottom=bottom, ceiling=ceiling)
        except Exception:
            pass

        try:
            if self.number_is_in_interval(number=0) and other < 0:
                bottom = self.bottom / other.ceiling
                ceiling = self.ceiling / other.ceiling
                return self.create_interval(bottom=bottom, ceiling=ceiling)
        except Exception:
            pass
        bottom = 0
        ceiling = 0
        return self.create_interval(bottom=bottom, ceiling=ceiling)

    def __eq__(self, other):
        if not isinstance(other, Interval):
            other = Interval(other)
        if self.bottom == other.bottom and self.ceiling == other.ceiling:
            return True
        else:
            return False

    def __ne__(self, other):
        if not isinstance(other, Interval):
            other = Interval(other)
        if self.bottom != other.bottom or self.ceiling != other.ceiling:
            return True
        else:
            return False

    def __lt__(self, other):
        if not isinstance(other, Interval):
            other = Interval(other)
        if self.bottom < other.bottom and self.ceiling < other.ceiling:
            return True
        elif self.bottom >= other.bottom and self.ceiling >= other.ceiling:
            return False
        else:
            raise Exception('Intervals can not be compared!')

    def __gt__(self, other):
        if not isinstance(other, Interval):
            other = Interval(other)
        if self.bottom > other.bottom and self.ceiling > other.ceiling:
            return True
        elif self.bottom <= other.bottom and self.ceiling <= other.ceiling:
            return False
        else:
            raise Exception('Intervals can not be compared!')

    def __le__(self, other):
        if not isinstance(other, Interval):
            other = Interval(other)
        if self.bottom <= other.bottom and self.ceiling <= other.ceiling:
            return True
        elif self.bottom >= other.bottom and self.ceiling >= other.ceiling:
            return False
        else:
            raise Exception('Intervals can not be compared!')

    def __ge__(self, other):
        if not isinstance(other, Interval):
            other = Interval(other)
        if self.bottom >= other.bottom and self.ceiling >= other.ceiling:
            return True
        elif self.bottom <= other.bottom and self.ceiling <= other.ceiling:
            return False
        else:
            raise Exception('Intervals can not be compared!')

    def number_is_in_interval(self, number: float) -> bool:
        """
        Checks if number is in interval

        :param number: float number
        :return: True or False
        """
        if self.bottom <= number <= self.ceiling:
            return True
        else:
            return False
