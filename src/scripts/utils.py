def check_if_float(inp):
    """
    Checks if input is a digit
    """
    if inp == '':
        return True

    if not (inp[0].isdigit() or inp[0] == '-'):
        return False

    if not inp.count('.') in [0, 1]:
        return False

    if not (inp.count('-') == 0 or (inp.count('-') == 1 and inp[0] == '-')):
        return False

    for spl in inp.split('.'):
        if not (spl.strip('-').isdigit() or spl.strip('-') == ''):
            return False

    return True


def validate_name_length(text: str):
    """
    Checks the length of the name
    """
    if len(text) < 17:
        return True
    return False


def from_rgb(rgb):
    """translates rgb tuple of int to a tkinter friendly color code"""
    r, g, b = rgb
    return f'#{r:02x}{g:02x}{b:02x}'
