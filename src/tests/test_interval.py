import pytest
from src.scripts.interval import Interval


def test_interval_init():
    interval1 = Interval(bottom=1, ceiling=3)
    interval2 = Interval(bottom=1, ceiling=-1)
    interval3 = Interval()
    interval4 = Interval(5)
    assert str(interval1) == '[1, 3]'
    assert str(interval2) == '[-1, 1]'
    assert str(interval3) == '[None, None]'
    assert str(interval4) == '[5, 5]'


def test_interval_str():
    interval1 = Interval(bottom=1, ceiling=3)
    assert str(interval1) == '[1, 3]'


def test_interval_repr():
    interval1 = Interval(bottom=1, ceiling=3)
    assert repr(interval1) == 'Interval(bottom=1, ceiling=3)'


def test_interval_create():
    interval1 = Interval.create_interval(bottom=1, ceiling=3)
    assert str(interval1) == '[1, 3]'


def test_range_width():
    interval1 = Interval.create_interval(bottom=1, ceiling=3)
    interval1.abs_range_width()
    assert interval1.range_width == 2


def test_range_median():
    interval1 = Interval.create_interval(bottom=5, ceiling=3)
    assert interval1.median == 4


def test_interval_add():
    interval1 = Interval(bottom=1, ceiling=3)
    interval2 = Interval(bottom=-3, ceiling=6)
    interval3 = interval1 + interval2
    interval4 = interval1 + 2
    assert str(interval3) == '[-2, 9]'
    assert str(interval4) == '[3, 5]'


def test_interval_sub():
    interval1 = Interval(bottom=1, ceiling=3)
    interval2 = Interval(bottom=-3, ceiling=6)
    interval3 = interval1 - interval2
    assert str(interval3) == '[-3, 4]'


def test_interval_mul():
    interval1 = Interval(bottom=1, ceiling=3)
    interval2 = Interval(bottom=-3, ceiling=6)
    interval3 = interval1 * interval2
    assert str(interval3) == '[-9, 18]'


def test_interval_truediv():
    interval1 = Interval(bottom=1, ceiling=3)
    interval2 = Interval(bottom=2, ceiling=7)
    interval3 = Interval(bottom=-5, ceiling=-1)
    interval4 = Interval(bottom=-8, ceiling=-2)
    interval5 = Interval(bottom=-7, ceiling=3)
    interval6 = interval1 / interval2
    interval7 = interval3 / interval4
    interval8 = interval5 / interval1
    interval9 = interval5 / interval3
    assert str(interval6) == '[0.42857142857142855, 0.5]'
    assert str(interval7) == '[0.125, 2.5]'
    assert str(interval8) == '[-7.0, 3.0]'
    assert str(interval9) == '[-3.0, 7.0]'
    assert str(interval5 / interval5) == '[0, 0]'


def test_interval_eq():
    interval1 = Interval(bottom=1, ceiling=3)
    interval2 = Interval(bottom=1.0, ceiling=3.0)
    interval3 = Interval(bottom=2, ceiling=7)
    assert interval1 == interval2
    assert not interval1 == interval3


def test_interval_ne():
    interval1 = Interval(bottom=1, ceiling=3)
    interval2 = Interval(bottom=1.0, ceiling=3.0)
    interval3 = Interval(bottom=2, ceiling=7)
    assert not interval1 != interval2
    assert interval1 != interval3


def test_interval_lt():
    interval1 = Interval(bottom=1, ceiling=3)
    interval2 = Interval(bottom=2, ceiling=4)
    interval3 = Interval(bottom=2, ceiling=3)
    interval4 = Interval(bottom=-2, ceiling=-1)

    assert interval1 < interval2
    assert not interval1 < interval4

    with pytest.raises(Exception, match='Intervals can not be compared!'):
        interval1 < interval3


def test_interval_gt():
    interval1 = Interval(bottom=2, ceiling=4)
    interval2 = Interval(bottom=1, ceiling=3)
    interval3 = Interval(bottom=3, ceiling=5)
    interval4 = Interval(bottom=1, ceiling=5)

    assert interval1 > interval2
    assert not interval1 > interval3

    with pytest.raises(Exception, match='Intervals can not be compared!'):
        interval1 > interval4


def test_interval_le():
    interval1 = Interval(bottom=1, ceiling=3)
    interval2 = Interval(bottom=2, ceiling=3)
    interval3 = Interval(bottom=2, ceiling=2)
    interval4 = Interval(bottom=-2, ceiling=-1)

    assert interval1 <= interval2
    assert not interval1 <= interval4

    with pytest.raises(Exception, match='Intervals can not be compared!'):
        interval1 <= interval3


def test_interval_ge():
    interval1 = Interval(bottom=2, ceiling=4)
    interval2 = Interval(bottom=2, ceiling=3)
    interval3 = Interval(bottom=3, ceiling=5)
    interval4 = Interval(bottom=1, ceiling=6)

    assert interval1 >= interval2
    assert not interval1 >= interval3

    with pytest.raises(Exception, match='Intervals can not be compared!'):
        interval1 >= interval4


def test_is_number_is_in_interval():
    interval1 = Interval(bottom=1, ceiling=3)
    assert interval1.number_is_in_interval(number=2)
    assert interval1.number_is_in_interval(number=1)
    assert interval1.number_is_in_interval(number=3)
    assert not interval1.number_is_in_interval(number=0)
    assert not interval1.number_is_in_interval(number=10)
