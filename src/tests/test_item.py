import pytest
from src.scripts.item import Parameter, MasterParameter, Item, MasterItem


@pytest.fixture()
def master_param_1():
    return MasterParameter(parameter_id=2, name='param', value_type=0, goal_value_type=10)


@pytest.fixture()
def param_1():
    return Parameter(parameter_id=1, value=10200)


@pytest.fixture()
def param_2():
    return Parameter(parameter_id=2, value=100)


@pytest.fixture()
def item_1(param_1, param_2):
    item = Item(name='item 1', fields=[], item_id=1)
    item.fields.append(param_1)
    item.fields.append(param_2)
    return item


@pytest.fixture()
def master_item_1(item_1, master_param_1):
    item = MasterItem(name='test', fields=[master_param_1, ])
    item.items.append(item_1)
    return item


def test_master_item_init(master_item_1):
    assert master_item_1.name == 'test'


def test_master_item_str(master_item_1):
    assert str(master_item_1) == 'test'


def test_master_item_repr(master_item_1):
    assert repr(master_item_1) == 'MasterItem(name="test", fields=MasterParameter(parameter_id=2, name="param", ' \
                                  'value_type="none", goal_value_type=10), items=Item(item_id=1, name="item 1", ' \
                                  'fields=Parameter(parameter_id=1, value=10200), Parameter(parameter_id=2, ' \
                                  'value=100)))'


def test_master_item_add_field(master_item_1):
    master_item_1.add_field(name='field 2', value_type=1, goal_value=10)
    assert repr(master_item_1) == 'MasterItem(name="test", fields=MasterParameter(parameter_id=2, name="param", ' \
                                  'value_type="none", goal_value_type=10), MasterParameter(parameter_id=0, ' \
                                  'name="field 2", value_type="float", goal_value_type=10), items=Item(item_id=1, ' \
                                  'name="item 1", fields=Parameter(parameter_id=1, value=10200), ' \
                                  'Parameter(parameter_id=2, value=100), Parameter(parameter_id=0, ' \
                                  'value=None)))'

    assert repr(master_item_1.items[0]) == 'Item(item_id=1, name="item 1", fields=Parameter(parameter_id=1, ' \
                                           'value=10200), Parameter(parameter_id=2, value=100), ' \
                                           'Parameter(parameter_id=0, value=None))'


def test_item_init(item_1):
    assert item_1.name == 'item 1'


def test_item_str(item_1):
    assert str(item_1) == 'item 1'


def test_item_repr(item_1):
    assert repr(item_1) == 'Item(item_id=1, name="item 1", fields=Parameter(parameter_id=1, ' \
                           'value=10200), Parameter(parameter_id=2, value=100))'


def test_master_parameter_init(master_param_1):
    assert master_param_1.name == 'param'


def test_master_parameter_str(master_param_1):
    assert str(master_param_1) == '2 - param'


def test_master_parameter_repr(master_param_1):
    assert repr(master_param_1) == 'MasterParameter(parameter_id=2, name="param", value_type="none", goal_value_type=10)'


def test_parameter_init(param_1):
    assert param_1.parameter_id == 1


def test_parameter_str(param_1):
    assert str(param_1) == '1 - 10200'


def test_parameter_repr(param_1):
    assert repr(param_1) == 'Parameter(parameter_id=1, value=10200)'
