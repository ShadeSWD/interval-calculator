import pytest
from src.scripts.utils import *


def test_check_if_float():
    assert check_if_float('')
    assert check_if_float('0')
    assert check_if_float('1')
    assert check_if_float('-1')
    assert check_if_float('1.1')
    assert check_if_float('-1.1')
    assert check_if_float('-')
    assert not check_if_float('-1-1')
    assert not check_if_float('1.1.1')
    assert not check_if_float('1a')
    assert not check_if_float('-1-1')
    assert not check_if_float('.1')
    assert not check_if_float('.')
    assert not check_if_float('a')
